const members = [
    {
        name: 'Gary',
        email: 'gary@gmail.com',
        status: 'active'
    },
    {
        name: 'Bob',
        email: 'Bob@gmail.com',
        status: 'inactive'
    },
]

module.exports = members;