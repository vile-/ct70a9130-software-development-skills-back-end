// https://github.com/bradtraversy/node_crash_course/blob/master/reference/http_demo.js

// A simple web server.
const http = require('http');

// Create server object
http.createServer((req, res) => {
    // Write response
    res.write('Hello World :3');
    res.end();
}).listen(5000, () => console.log('Server running...')
)